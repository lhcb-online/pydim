#!/bin/bash
cd ./doc/guide
make html

cd ../../

destination_sphinx_doc=./public/guide

echo "Creating sphinx doc destination directory in $destination_sphinx_doc"

mkdir -p $destination_sphinx_doc

echo "Done."

echo "Copying sphinx doc to the destination directory $destination_sphinx_doc"

cp -R ./doc/guide/.build/html/* $destination_sphinx_doc

echo "Done."

ls ./public
