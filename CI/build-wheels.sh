#!/bin/bash
set -e -u -x
PLAT=manylinux2014_x86_64
NAME=pydim3

function repair_wheel {
    wheel="$1"
    if ! auditwheel show "$wheel"; then
        echo "Skipping non-platform wheel $wheel"
    else
        auditwheel repair "$wheel" --plat "$PLAT" -w ./wheelhouse-manylinux
    fi
}


# Install a system package required by our library
echo "We are here $(pwd)!"
cp -ar CI/daq40.repo /etc/yum.repos.d
yum install -y dim dim-devel

# Compile wheels
for PYBIN in /opt/python/*/bin; do
#    "${PYBIN}/pip" install -r /io/dev-requirements.txt
    if ! echo ${PYBIN} | grep -q "pypy"; then
        "${PYBIN}/pip" wheel ./ --no-deps -w wheelhouse/
    else
        echo "Skipping platform $PYBIN"
    fi
done

# Bundle external shared libraries into the wheels
for whl in wheelhouse/*.whl; do
    repair_wheel "$whl"
done

# Install packages and test
#for PYBIN in /opt/python/*/bin/; do
#    "${PYBIN}/pip" install python-manylinux-demo --no-index -f /io/wheelhouse
#    (cd "$HOME"; "${PYBIN}/nosetests" pymanylinuxdemo)
#done
