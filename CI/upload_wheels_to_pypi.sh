#!/bin/bash
VERSION=`cat VERSION`
#WHEELS=`ls /wheelhouse-manylinux/*$VERSION*.whl` 2>/dev/null
for PYVER in $(ls /opt/python)
do
   if ! echo ${PYVER} | grep -q "pypy"; then
        /opt/python/${PYVER}/bin/pip install --prefix=/opt/python/${PYVER} twine
        WHEEL=$(ls ./wheelhouse-manylinux/*${VERSION}*${PYVER}*.whl)
        #echo "----------------------------------------> /opt/python/${PYVER}/bin/twine upload -u ${PYPI_USERNAME} -p ${PYPI_PASSWORD} --repository $1 ${WHEEL}"
        /opt/python/${PYVER}/bin/twine upload -u ${PYPI_USERNAME} -p ${PYPI_PASSWORD} --repository $1 ${WHEEL}
        
    else
        echo "Skipping platform $PYBIN"
    fi
done
