#!/bin/bash
cd ./doc/api/
doxygen
cd ../../

destination_doxygen_doc=./public/api

echo "Creating doxygen doc destination directory in $destination_doxygen_doc"

mkdir -p $destination_doxygen_doc

echo "Done."

echo "Copying doxygen doc to destination directory $destination_doxygen_doc"

cp -R ./doc/api/html/* $destination_doxygen_doc

echo "Done."
