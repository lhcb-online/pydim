#!/bin/bash
rm -R public/*

VERSION=`cat VERSION`
TAR_GZ=`ls dist/*$VERSION*.tar.gz` 2>/dev/null
RPM=`ls dist/*$VERSION*.rpm` 2>/dev/null

mkdir public/$VERSION
cp $TAR_GZ public/$VERSION/
cp $RPM public/$VERSION/
