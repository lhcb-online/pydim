#!/bin/bash
VERSION=`cat VERSION`
TAR_GZ=`ls dist/*$VERSION*.tar.gz` 2>/dev/null
twine upload --repository $1 $TAR_GZ
