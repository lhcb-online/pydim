%define name pydim
%define version 2.1.0
%define unmangled_version 2.1.0
%define unmangled_version 2.1.0
%define release 1

Summary: Python interface package for DIM C and C++ interfaces.
Name: %{name}
Version: %{version}
Release: %{release}
Source0: %{name}-%{unmangled_version}.tar.gz
License: GPL
Group: LHCb/Online
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
Vendor: Niko Neufeld (originally by Radu Stoica) <niko.neufeld@cern.ch>
Packager: Niko Neufeld <niko.neufeld@cern.ch>
Requires: dim python
Url: http://lhcbdoc.web.cern.ch/lhcbdoc/pydim/index.html
Distribution: centos7
BuildRequires: python-devel dim-devel

%description
The PyDIM package exposes the DIM C and C++ functions and classes in Python.

%prep
%setup -n %{name}-%{unmangled_version} -n %{name}-%{unmangled_version}

%build
env CFLAGS="$RPM_OPT_FLAGS" python setup.py build

%install
python setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES

%clean
rm -rf $RPM_BUILD_ROOT

%files -f INSTALLED_FILES
%defattr(-,root,root)
%doc INSTALL doc/guide
