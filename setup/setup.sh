#!/bin/bash
pathmunge () {
        if ! echo $PATH | /bin/egrep -q "(^|:)$1($|:)" ; then
           if [ "$2" = "after" ] ; then
              export PATH=$PATH:$1
           else
              export PATH=$1:$PATH
           fi
        fi
}

ldpathmunge () {
        if ! echo $LD_LIBRARY_PATH | /bin/egrep -q "(^|:)$1($|:)" ; then
           if [ "$2" = "after" ] ; then
              if ! test -z $LD_LIBRARY_PATH; then                 
                  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$1
              else
                  export LD_LIBRARY_PATH=$1
              fi
           else
              if ! test -z $LD_LIBRARY_PATH; then
                  export LD_LIBRARY_PATH=$1:$LD_LIBRARY_PATH
              else
                  export LD_LIBRARY_PATH=$1
              fi
           fi
        fi
}

luname=$(uname |tr "[:upper:]" "[:lower:]")
export PYTHONSTARTUP=`pwd`/startup.py
case $luname in 
 	darwin)
		export PYTHONPATH=$PYTHONPATH:`pwd`/python:`pwd` ;;
 	linux)
		export PYTHONPATH=$PYTHONPATH:`pwd`/python:`pwd` ;;
esac

###############################################################################
# modify below to point to the dim directory
export DIMHOME=`pwd`/../dim && echo "DIMHOME set to: "$DIMHOME
# ^ 
##############################################################################
export DIM_DNS_NODE=localhost && echo "DIM_DNS_NODE set to: "$DIM_DNS_NODE

pathmunge  $DIMHOME/$luname 
ldpathmunge  $DIMHOME/$luname 

cd python
ln -sf ../build/lib/dimc.so .
ln -sf ../build/lib/dimcpp.so .
cd ..

dns &
did &


