PyDIM
=====

PyDIM is a Python interface to [DIM](http://dim.web.cern.ch). PyDIM lets you create DIM clients
and servers, using an API very similar to the one that is used for C/C++.

Check the online documentation at:

    http://lhcbdoc.web.cern.ch/lhcbdoc/pydim/index.html


Installation
============

PyDIM can be installed by following the installation documentation situated at :

    http://lhcbdoc.web.cern.ch/lhcbdoc/pydim/api/index.html

Hacking
=======

Here are some guidelines that may help if you want to modify or just read the
code of pydim.

Directory structure
-------------------

src:
    The part of the extension written in C++
doc:
    Documentation
examples:
    Examples for how to write servers and clients with PyDIM
pydim:
    Additional functions included in the extension, written in Python.
dimbrowser:
    Contains the Python wrapper to the C++ class DimBrowser
setup:
    An old setup script. It should be deprecated.
tests:
    Unit tests. They can be used as a reference.
CI:
    Contains the CI files needed to the Continuous Integration of PyDIM.
examples:
    Examples of how PyDIM fonctionnalities work


The following files are included in the root directory:

INSTALL:
    Instructions for installing and building the RPM

MANIFEST.in:
    A template for the Manifest file used with `distutils`.

setup.cfg:
    Additional configuration for the `distutils` script.


=======
## Changelog

3.0.6
-----

* Rebuilt and tested successfully with Python 3.9 and RHEL9. Windows build broken

3.0.3
-----

* Make it available for Python 3.7

3.0.1
-----

* Changing the python_requires in the setup.py file.

3.0.0
-----

* PyDIM is now compatible with Python 3.6

2.1.0.419397
------------

* The description string (http://lhcbdoc.web.cern.ch/lhcbdoc/pydim/guide/pydim-c.html#description-string) that had to be passed to the following functions:
- dic_info_service
- dic_cmnd_service
- dic_cmnd_callback
- dic_sync_info_service
- dic_sync_cmnd_servicez
is not mandatory anymore. Added a DNS cache system : http://lhcbdoc.web.cern.ch/lhcbdoc/pydim/guide/pydim-c.html#dns-cache

* Adding dimbrowser Python module in order to use the DimBrowser C++ class with Python code :
 http://lhcbdoc.web.cern.ch/lhcbdoc/pydim/guide/dimbrowser.html

Contact
=======

This module was originally developed by Radu Stoica based on code by Niko Neufeld.
Juan Manuel Caicedo improved it significantly and fixed many bugs.

It is currently maintained by Niko Neufeld (niko.neufeld@cern.ch)

Feel free to send your questions and bug reports.
