%define name pydim
%define version 1.3.5
%define release 1
%define distribution  %(cat /etc/redhat-release | grep -q "5" && echo slc5)%(cat /etc/redhat-release | grep -q '4' && echo 'slc4')%(cat /etc/redhat-release | grep -q 'SL release 4' && echo 'slc4')

Summary: Python interface package for DIM C and C++ interfaces.
Name: %{name}
Version: %{version}
Release: %{release}.%{distribution}
Source0: %{name}-%{version}.tar.gz
License: GPL
Group: LHCb/Online
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
Vendor: Niko Neufeld <niko.neufeld@cern.ch>
Packager: Niko Neufeld <niko.neufeld@cern.ch>
Requires: dim python
Url: http://lbdoc.cern.ch/pydim/
BuildRequires: python-devel dim-devel

%description
The PyDIM package exposes the DIM C and C++ functions and classes in Python.

%prep
%setup

%build
env CFLAGS="$RPM_OPT_FLAGS" python setup.py build

%install
python setup.py install --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES
if [ %{distribution} = 'slc5' ]; then  sed -i 's/\(.*\)pyc/\1pyo\n\1pyc/' INSTALLED_FILES; fi
%clean
rm -rf $RPM_BUILD_ROOT

%files -f INSTALLED_FILES
%defattr(-,root,root)
%doc INSTALL doc/guide README
