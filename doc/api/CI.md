Continuous Integration of PyDIM
===============================

Introduction
============
Continuous Integration (CI) is a very good practice to automate things that need to be done after adding some modifications in a project. For example, CI can be used to generate the documentation associated to the project and to upload it on web servers. We can also package the project and make it available for final users. All these things are done in an automated way thanks to the CI.

**Note : All the CI necessary files are located in the CI folder in the root of PyDIM project**

Actions performed by the Continuous Integration of PyDIM
===============================
The following actions are automated by the CI of PyDIM :
+ The build of PyDIM
+ The generation and the upload of the documentation (manual)
+ The packaging of PyDIM and the upload of PyDIM on PyPI.org (manual)

After a push on the correct **integration_pythonVersion** branch, gitlab-ci will create jobs according to the .gitlab-ci.yml situated in the PyDIM project's root folder.

The instructions within these jobs will be executed on a **Runner** : a Docker image container. If a job fails, the user will be notified by email and by the UI of the Gitlab CI accessible via the CI / CD tab.

How PyDIM Continuous Integration works ?
========================================

As the PyDIM Project is stored on the CERN's Gitlab, the CI tool provided by Gitlab is used. This tool is **gitlab-ci**.
Everything starts after a push in the **integration_pythonVersion** branch. After pushing in the Integration branch, gitlab-ci will perform actions according to a file situated in the PyDIM project's root : .gitlab-ci.yml.

Actions are performed by a **Runner**. This runner is a machine that run the jobs written in the .gitlab-ci.yml file and sends the results to Gitlab. The runner can be either a **Docker** image or a specific machine created for that.

The PyDIM's CI runner
----------------------------------

For PyDIM's Continous Integration, a **Docker** image has been choosen to be the runner. Indeed, it is very easy to make a Docker image and to upload it to the CERN's Gitlab Docker Container Registry.

Docker is an open platform for developers and sysadmins to build, ship and run distributed applications, wether on laptops, data center VMs, or on the cloud (source : [https://www.docker.com/](https://www.docker.com/)).

If you are not familiar with Docker, it is better to read the [Docker's get started guide](https://docs.docker.com/get-started/) before continuing to read this documentation.

### The creation of the PyDIM Docker image ###

Before creating a Docker image, Docker needs to be installed on your machine. The Docker Community Edition has been used and can be downloaded [here](https://www.docker.com/community-edition).

All files needed to create the PyDIM Docker image are situated in the CI/docker_Python_Version folder of the PyDIM project. This folder contains :
+ A file *Dockerfile* that contains all the instructions to create the PyDIM Docker image
+ A file *packages.txt* that contains the packages to install with yum to the PyDIM Docker image

A Docker Image has to be created to run all the CI commands on the PyDIM project (e.g. build, documentation...). So, this image has to have the DIM system installed following the instructions [here](http://dim.web.cern.ch/dim/dim_unix.html). It is done by the commands included in the Dockerfile.

#### The Dockerfile ####

The Dockerfile is commented to make it more understandable, but here are the main parts of this Dockerfile :
+ Inherit the PyDIM Docker image from the base image [Centos7 Python 2.7](https://hub.docker.com/r/centos/python-27-centos7/)
+ Create a directory /dim in the Docker image so that the DIM files could be copied from the host machine to this folder
+ Add **http proxys** in order to allow our Docker image to access internet to download all needed packages from the yum command (optional)
+ Install with pip the packages that are situated in the pip-packages.txt file
+ Install with yum the packages that are situated in the packages.txt file
+ Install DIM (the DIM folder path has to be set)

#### Create a Docker image from the Dockerfile ####
To build the Docker image from the Dockerfile, go to the docker_Python_Version folder and type the command :

    bash-4.2$ sudo docker build -t name_of_pydim_image .

The name_of_pydim_image can be replaced by the name you want to give. This command builds a Docker container so that the image will be executed in it.

If an **http_proxy** has to be used to build the image (yum, pip, ...), just pass the http_proxy and https_proxy arguments :

    bash-4.2$ sudo docker build -t name_of_pydim_image --build-arg http_proxy=netgw01:8080 --build-arg https_proxy=netgw01:8080 .

if both http and https proxy have the same address :

    bash-4.2$ sudo docker build -t name_of_pydim_image --build-arg proxy=netgw01:8080 .

If everything works, you can **run the container** created using the command :

    bash-4.2$ sudo docker run -it name_of_pydim_image bash

This will run a bash that will be executed on the Docker image.

### Upload / push the PyDIM Docker image to the CERN's Gitlab Docker Container Registry ###

Only three steps are needed to push the PyDIM Docker image to the CERN's Gitlab Docker Container Registry.

First, log in to the CERN's Docker Container Registry :

    bash-4.2$ sudo docker login gitlab-registry.cern.ch

Second, build the container of the PyDIM Docker image (if you need to have http_proxys to build it, go to the previous section). A tag should be added to the name of the Docker image according to the Python version of PyDIM that has to be built :
+ tag = python2.7 for PyDIM 2.X.X
+ tag = python3.6 for PyDIM 3.X.X

You have to follow the following naming convention for your image in order to make the CI work :

    gitlab-registry.cern.ch/lhcb-online/pydim:tag

To build the container of the PyDIM Docker image (example for python2.7 docker image), go to the CI/docker_Python_2.7 folder and type :

    bash-4.2$ sudo docker build -t gitlab-registry.cern.ch/lhcb-online/pydim:python2.7 .

Finally, push the container image of the PyDIM Docker to the CERN's Gitlab Docker Container Registry :

    bash-4.2$ sudo docker push gitlab-registry.cern.ch/lhcb-online/pydim:python2.7

Note: If you need to push a new version of your container, it is preferred to delete the old one before doing it. This can be done directly from the CERN's Gitlab UI in the **Registry** tab.

For more informations about the Gitlab Container Registry, visit this link [https://gitlab.cern.ch/help/user/project/container_registry](https://gitlab.cern.ch/help/user/project/container_registry).

Configure PyDIM CI : the gitlab-ci.yml file
--------------------------------------------

This file contains all the jobs that have to be executed by gitlab-ci. Its structure is as follow :
+ Indicate to gitlab-ci which runner has to be used to execute the jobs
+ Define stages
+ Define jobs

### stages ###
A stage is a gathering of jobs. The jobs inside a same stage are **launched** at the same time on different runners.

### jobs ###
A job executes a bash command on the runner on which it has been triggered.

### Passing files / folder between jobs and stages
In order to pass some folders generated by a job (e.g. the build folder) to another job, it needs to be declared as an artifact :

    build_job:
      stage: build #belongs to the build stage
      script: #script to execute
        - bash CI/build.sh
      artifacts: #folders that will be passed to future jobs
        paths:
          - build

For more informations about the configuration of the .gitlab-ci.yml file, please visit this link [https://docs.gitlab.com/runner/executors/docker.html#define-image-and-services-from-gitlab-ci-yml](https://docs.gitlab.com/runner/executors/docker.html#define-image-and-services-from-gitlab-ci-yml).

Executed jobs
=============

This part will explain how each PyDIM CI jobs works.

### build_job ###

This job executes the CI/build.sh script.

As PyDIM is written in C / Python, the file setup.py is needed to build and package the project. The command that does it is :

    python setup.py build bdist_wheel bdist_rpm sdist

This will create a build folder and a dist folder which contains respectively the built application and the package of PyDIM (a .whl, .tar.gz and a .rpm file).

### doc_sphinx_job ###

This job executes the CI/doc_sphinx.sh script.

It simply does a *make html* in the doc/guide folder and copies the generated files in the /public directory so that these files could be uploaded on a public EOS folder.

### doc_doxygen_job ###
This job executes the CI/doc_doxygen.sh script.

This scripts moves to the /doc/api folder and executes :

    bash-4.2$ doxygen

Then, it copies the generated files to the /public directory so that these files could be uploaded on a public EOS folder.

### doc_home_job ###
This job executes the CI/doc_home.sh script.

This script copies the doc/home_doc files in the /public directory so that these files could be uploaded on a public EOS folder.

### upload_doc_job (manual)###
This job is executed on an other Docker image created by the CERN. It allows to upload the files that are in the /public directory in an EOS folder.

This image is *gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest*.

In order to make this work, variables has to be defined in the Settings of Gitlab's CI /CD :

- EOS_ACCOUNT_USERNAME
- EOS_ACCOUNT_PASSWORD

These variables stores the username and the password of the Service Account **lbpydim** so that the uploading on the EOS public folder works (lbpydim has the rights to copy on these folders).

Another variable has to be defined in the job description in the *.gitlab-ci.yml* file : EOS_PATH that refers to the path where the files located in the /public folder have to be copied. For pydim, the public EOS directory is :

    /eos/project/l/lhcbwebsites/www/projects/pydim/

After these variables set, the deploy-eos bash command is executed. All files are copied automatically.

### upload_package_to_EOS_job (manual)###
This job works the same as the **upload_doc_job**. It copies the pydim's packages files in an EOS directory.

### upload_package_to_pypi_job (manual)###
This job executes the CI/upload_package_to_pypi.sh that takes the name of the repository as a parameter (here pypi)

This job get the version of PyDIM from the *VERSION* file, and get the dist/\*VERSION\*.tar.gz file in order to upload it to [pypi.org](https://pypi.org/) with the command [twine](https://pypi.org/project/twine/).

### upload_package_to_testpypi_job (manual)###
This job executes the CI/upload_package_to_pypi.sh that takes the name of the repository as a parameter (here testpypi)

This job get the version of PyDIM from the *VERSION* file, and get the dist/\*VERSION\*.tar.gz file in order to upload it to [testpypi.org](https://test.pypi.org/) with the command [twine](https://pypi.org/project/twine/).

All these upload jobs has to be started manually, indeed it can be possible that we do not want to push PyDIM in production without having tested that it works well !

**Note : Python2.7 jobs are executed on the Runner that has the Python2.7 tag and Python3.6 jobs are executed on the Runner that has the Python3.6 tag.**
