import pydim
from threading import Thread
import time

#callback executed when dis_update_service is called
def server_example_cmnd_callback(value,tag):
    print("Received from the client : {0}".format(value[0]))

#Class that defines a DIM Server
class Server(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.stopped = False
        self.create_server_cmnd()
        self.startServer()

    #Create a service that gives the time
    def create_server_cmnd(self):
        self.cmnd = pydim.dis_add_cmnd('example-command',"C",server_example_cmnd_callback,1)
        if not self.cmnd:
            sys.exit(1)

    #Start the DIM Server
    def startServer(self):
        pydim.dis_start_serving("ExampleCommand-server")

    #Stop the DIM Server
    def stop(self):
        self.stopped = True
        self.join()

    # Method that runs when server.start() is called
    def run(self):
        while not self.stopped:
            time.sleep(.1)

class Client:

    def call_example_command(self):
        #arg must be a tuple
        args = ("Hello world !",)
        pydim.dic_cmnd_service("example-command",args,"C")

def stop_server(server):
    print("Stopping server...")
    server.stop()
    print("Server stopped")

def menu():
    print("------- Using cache example -------")
    print("1. Deactivate command format cache")
    print("2. Activate command format cache")
    print("3. Call a command on the server")
    print("4. Exit")
    print("-----------------------------------")
    print("")

def main():
    print("Creating server")
    server = Server()

    print("Starting server")
    server.start()

    exited = False

    client = Client()

    while not exited:
        menu()
        choice = raw_input("Choice : ")
        if choice == "1":
            pydim.deactivate_command_format_cache()
            print("Cache has been deactivated")
        elif choice == "2":
            pydim.activate_command_format_cache()
            print("Cache has been activated")
        elif choice == "3":
            client.call_example_command()
            time.sleep(1)
        elif choice == "4":
            stop_server(server)
            exited = True
        else:
            print("Wrong input !")

    print("Goodbye")


if __name__ == '__main__':
    main()
