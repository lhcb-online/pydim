#!/bin/env python
# -*- coding: UTF-8 -*-
"""
An example for showing how a client can run the commands on a DIM server.

"""

import sys
import math
import random

# Import the pydim module
import pydim

def command1():

    #f = random.choice([math.pi, math.e, 42.0])
    #The argument must be a tuple
    args = (600.8,200.6,300.5)
    #print "Calling command 1. Arguments: %s" % args
    res = pydim.dic_cmnd_service("example-command-1",args)
    #res = pydim.dic_cmnd_service("example-command-1", args, "F")

def command2():
    n = random.choice(list(range(5)))
    text = random.choice(["hola", "hi", "bonjour"])

    args = (n, text)
    print("Calling command 2. Arguments: %s, %s" % args)
    res = pydim.dic_cmnd_service("example-command-2", args)
    #res = pydim.dic_cmnd_service("example-command-2", args, "I:1;C")

def command_callback_success_or_fail(tag,ret_code):
    print(("command_callback_success_or_fail, tag = {0}, ret_code = {1}".format(tag,ret_code)))

def command_callback():
    n = random.choice(list(range(5)))
    text = random.choice(["hola", "hi", "bonjour"])

    args = (n,text)
    print("Calling command_callback. Arguments: %s, %s" % args)
    res = pydim.dic_cmnd_callback("example-command-callback",args,command_callback_success_or_fail,4)

def command_sync():
    number_of_seconds_to_wait = eval(input("Enter the number of seconds to wait :"))
    res = pydim.dic_sync_cmnd_service("example-command-sync",(int(number_of_seconds_to_wait),),"I")
    print(("Result of command = %s" % res))


def help():
    print("""This is a DIM client for the commands example server.

The following options are available:

    1     Run command 1
    2     Run command 2
    3     Run command_callback
    4     Run synchronized command
    H     Display this help text
    Q     Exit this program

    """)

def main():

    help()

    exit = False
    while not exit:

        action = input("Action (Press 'Q' to exit): ")

        if action == "1":
            command1()
        elif action == "2":
            command2()
        elif action == "3":
            command_callback()
        elif action == "4":
            command_sync()
        elif action == "Q":
            exit = True
            print("Bye!")
        elif action == "H":
            help()

if __name__ == "__main__":
    main()
