VERSION=$(cat ./VERSION)
PYTHON=$(DIM_PYTHON)
ifeq ($(PYTHON), )
	PYTHON=python3
endif

all: build

build: clean
	$(PYTHON) setup.py build ${OPTIONS}

install:
	$(PYTHON) setup.py install
	doc_api
	mkdir -p /usr/share/doc/PyDIM-${VERSION}
	cp -rf doc/* /usr/share/doc/PyDIM-${VERSION}

clean:
	$(PYTHON) setup.py clean
	@rm -rf PyDIM-${VERSION}.tar.gz rpm build doc/api/html doc/api/latex

rpm: clean doc
	@echo "Building PyDIM package version ${VERSION}"
	$(PYTHON) setup.py bdist_rpm
	@echo "All done. RPMs located in dist/"

msi: clean
	$(PYTHON) setup.py bdist_wininst

test:
	gcc -g -o tests/stress tests/stress.c -ldim -lpthread

doc_api_clean:
	@rm -rf doc/api/html doc/api/latex

doc_guide_clean:
	@cd doc/guide && make clean

doc_guide:build
	@cd doc/guide && make html

doc_api: doc_api_clean
	@cd doc/api && doxygen

doc_clean: doc_api_clean doc_guide_clean
	@echo "Cleaning documentation"

doc: doc_clean doc_api doc_guide
	@echo "Generating documentation"

