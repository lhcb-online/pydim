#!/bin/env python
# -*- coding: utf-8 -*-
"""
This is a Python client unittest realized with unittest.
Please launch a server situated in the unit_tests/servers before launching these unit tests (or they will all fail !)
"""
import unittest
import time
import os
import sys

import pydim

SERVER_NAME="server_test_pydim"

'''
SERVICES Tests
'''

INT_SERVICE_NAME="int_service"
INT_SERVICE_VALUE_EXPECTED = (42,)

CHAR_SERVICE_NAME = "char_service"
CHAR_SERVICE_VALUE_EXPECTED = ("a",)

STRING_SERVICE_NAME = "string_service"
STRING_SERVICE_VALUE_EXPECTED = ("Hello world !",)

LONG_SERVICE_NAME = 'long_service'
LONG_SERVICE_VALUE_EXPECTED = (2147483647,)

SHORT_SERVICE_NAME = 'short_service'
SHORT_SERVICE_VALUE_EXPECTED = (32767,)

DOUBLE_SERVICE_NAME = 'double_service'
DOUBLE_SERVICE_VALUE_EXPECTED = (42424242.42,)

FLOAT_SERVICE_NAME = "float_service"
FLOAT_SERVICE_VALUE_EXPECTED = (42.42,)

XTRALONG_SERVICE_NAME = "xtra_long_service"
XTRALONG_SERVICE_VALUE_EXPECTED = (9223372036854775807,)

STRING_INT_SERVICE_NAME = "string_int_service"
STRING_INT_SERVICE_VALUE_EXPECTED = ("Hello world !",42)

INT_STRING_SERVICE_NAME = "int_string_service"
INT_STRING_SERVICE_VALUE_EXPECTED = (42,"Another string to try if it works or not !")

ALL_VALUES_SERVICE_NAME = "all_values_service"
ALL_VALUES_SERVICE_VALUE_EXPECTED = (42,'a',"Hello world !",2147483647,32767,42424242.42,9223372036854775807,"Hello world !",42,42,"Another string to try if it works or not !")

BYTES_SERVICE_NAME = "bytes_service"
BYTES_SERVICE_VALUE_EXPECTED = (b'as\xd8\xe1\xb7\xeb\xa8\xe5 \xd2\xb7\xe1',)

BYTES_STRING_INT_SERVICE_NAME = "bytes_string_int_service"
BYTES_STRING_INT_SERVICE_VALUE_EXPECTED = ('Hello world in bytes',"Hello world !",42)

class PydimDicInfoServiceTestCase(unittest.TestCase):
    def run_test(self,service_name,value_expected):
        value = pydim.dic_sync_info_service(service_name,None,2)
        if value == ():
            self.fail("The server is down or the service "+service_name+" has not been implemented in the server.")
        else:
            self.assertEqual(value,value_expected)

    def test_int_service(self):
        self.run_test(INT_SERVICE_NAME,INT_SERVICE_VALUE_EXPECTED)

    def test_char_service(self):
        self.run_test(CHAR_SERVICE_NAME,CHAR_SERVICE_VALUE_EXPECTED)

    def test_string_service(self):
        self.run_test(STRING_SERVICE_NAME,STRING_SERVICE_VALUE_EXPECTED)

    def test_long_service(self):
        self.run_test(LONG_SERVICE_NAME,LONG_SERVICE_VALUE_EXPECTED)

    def test_short_service(self):
        self.run_test(SHORT_SERVICE_NAME,SHORT_SERVICE_VALUE_EXPECTED)

    def test_double_service(self):
        self.run_test(DOUBLE_SERVICE_NAME,DOUBLE_SERVICE_VALUE_EXPECTED)

    def test_float_service(self):
        value = pydim.dic_sync_info_service(FLOAT_SERVICE_NAME,None,2)
        if value is not None:
            #rounding value because float precision is bad and the test fail
            self.assertEqual((round(value[0],2),),FLOAT_SERVICE_VALUE_EXPECTED)
        else:
            self.fail("The server is down or the service "+FLOAT_SERVICE_NAME+" has not been implemented in the server.")

    def test_xtralong_service(self):
        self.run_test(XTRALONG_SERVICE_NAME,XTRALONG_SERVICE_VALUE_EXPECTED)

    def test_string_int_service(self):
        self.run_test(STRING_INT_SERVICE_NAME,STRING_INT_SERVICE_VALUE_EXPECTED)

    def test_int_string_service(self):
        self.run_test(INT_STRING_SERVICE_NAME,INT_STRING_SERVICE_VALUE_EXPECTED)

    def test_all_values_service(self):
        value = pydim.dic_sync_info_service(ALL_VALUES_SERVICE_NAME,None,10)
        self.assertEqual(value,ALL_VALUES_SERVICE_VALUE_EXPECTED)

    def test_bytes_service(self):
        self.run_test(BYTES_SERVICE_NAME,BYTES_SERVICE_VALUE_EXPECTED)

    def test_bytes_string_int_service(self):
        self.run_test(BYTES_STRING_INT_SERVICE_NAME,BYTES_STRING_INT_SERVICE_VALUE_EXPECTED)
'''
END OF SERVICES TESTS
'''

'''
COMMANDS TESTS, you have to check that the commands parameters are displayed in the server side
'''

INT_CMND_NAME = 'int_cmnd'
INT_CMND_PARAMS = (42,)

CHAR_CMND_NAME = "char_cmnd"
CHAR_CMND_PARAMS = ("C",)

STRING_CMND_NAME = "string_cmnd"
STRING_CMND_PARAMS = ("Hello world !",)

LONG_CMND_NAME = "long_cmnd"
LONG_CMND_PARAMS = LONG_SERVICE_VALUE_EXPECTED

SHORT_CMND_NAME = "short_cmnd"
SHORT_CMND_PARAMS = SHORT_SERVICE_VALUE_EXPECTED

DOUBLE_CMND_NAME = "double_cmnd"
DOUBLE_CMND_PARAMS = DOUBLE_SERVICE_VALUE_EXPECTED

FLOAT_CMND_NAME = "float_cmnd"
FLOAT_CMND_PARAMS = FLOAT_SERVICE_VALUE_EXPECTED

XTRALONG_CMND_NAME = "xtralong_cmnd"
XTRALONG_CMND_PARAMS = XTRALONG_SERVICE_VALUE_EXPECTED

STRING_INT_CMND_NAME = "string_int_cmnd"
STRING_INT_CMND_PARAMS = STRING_INT_SERVICE_VALUE_EXPECTED

INT_STRING_CMND_NAME = "int_string_cmnd"
INT_STRING_CMND_PARAMS = INT_STRING_SERVICE_VALUE_EXPECTED

ALL_VALUES_CMND_NAME = "all_values_cmnd"
ALL_VALUES_CMND_PARAMS = ALL_VALUES_SERVICE_VALUE_EXPECTED

BYTES_CMND_NAME = "bytes_cmnd"
BYTES_CMND_PARAMS = BYTES_SERVICE_VALUE_EXPECTED

BYTES_STRING_INT_CMND_NAME = "bytes_string_int_cmnd"
BYTES_STRING_INT_CMND_PARAMS = BYTES_STRING_INT_SERVICE_VALUE_EXPECTED

CLASS_INT_CMND_NAME = "class_int_cmnd"
CLASS_INT_CMND_PARAMS = (33,)


class PydimDicCmndServiceTestCase(unittest.TestCase):

    def run_test(self,cmnd_name,params):
        retcode = pydim.dic_cmnd_service(cmnd_name,params)
        if retcode == 0:
            self.fail("The server is not online or the command "+cmnd_name+" has not been implemented in the server")
        else:
            self.assertEqual(retcode,1)

    def test_int_cmnd(self):
        self.run_test(INT_CMND_NAME,INT_CMND_PARAMS)

    def test_char_cmnd(self):
        self.run_test(CHAR_CMND_NAME,CHAR_CMND_PARAMS)

    def test_string_cmnd(self):
        self.run_test(STRING_CMND_NAME,STRING_CMND_PARAMS)

    def test_long_cmnd(self):
        self.run_test(LONG_CMND_NAME,LONG_CMND_PARAMS)

    def test_short_cmnd(self):
        self.run_test(SHORT_CMND_NAME,SHORT_CMND_PARAMS)

    def test_double_cmnd(self):
        self.run_test(DOUBLE_CMND_NAME,DOUBLE_CMND_PARAMS)

    def test_float_cmnd(self):
        self.run_test(FLOAT_CMND_NAME,FLOAT_CMND_PARAMS)

    def test_xtralong_cmnd(self):
        self.run_test(XTRALONG_CMND_NAME,XTRALONG_CMND_PARAMS)

    def test_string_int_cmnd(self):
        self.run_test(STRING_INT_CMND_NAME,STRING_INT_CMND_PARAMS)

    def test_int_string_cmnd(self):
        self.run_test(INT_STRING_CMND_NAME,INT_STRING_CMND_PARAMS)

    def test_all_values_cmnd(self):
        self.run_test(ALL_VALUES_CMND_NAME,ALL_VALUES_CMND_PARAMS)

    def test_bytes_cmnd(self):
        self.run_test(BYTES_CMND_NAME,BYTES_CMND_PARAMS)

    def test_bytes_string_int_cmnd(self):
        self.run_test(BYTES_STRING_INT_CMND_NAME,BYTES_STRING_INT_CMND_PARAMS)

    def test_class_int_cmnd(self):
        for i in range(10):
            self.run_test(CLASS_INT_CMND_NAME, CLASS_INT_CMND_PARAMS)


'''
END OF COMMANDS TESTS
'''

if __name__ == "__main__":
    dns_node = os.getenv("DIM_DNS_NODE")
    #change the value of the DNS node if necessary by adding the line "dns_node = name_of_dns_node"
    if dns_node is not None:
        pydim.dis_set_dns_node(dns_node)

    if not pydim.dis_get_dns_node():
        print("No Dim DNS node found. Please set the environment variable DIM_DNS_NODE")
        sys.exit(1)

    loader = unittest.TestLoader()

    test_suite = unittest.TestSuite()

    test_suite.addTests(loader.loadTestsFromTestCase(PydimDicInfoServiceTestCase))
    test_suite.addTests(loader.loadTestsFromTestCase(PydimDicCmndServiceTestCase))

    unittest.TextTestRunner(verbosity=1).run(test_suite)
