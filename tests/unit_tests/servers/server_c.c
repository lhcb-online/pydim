#include <dis.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/*
* SERVICES DEFINITION
*/
#define INT_SERVICE_NAME "int_service"
#define INT_SERVICE_FORMAT "I:1"

#define CHAR_SERVICE_NAME "char_service"
#define CHAR_SERVICE_FORMAT "C:1"

#define STRING_SERVICE_NAME "string_service"
#define STRING_SERVICE_FORMAT "C"

#define LONG_SERVICE_NAME "long_service"
#define LONG_SERVICE_FORMAT "L"

#define SHORT_SERVICE_NAME "short_service"
#define SHORT_SERVICE_FORMAT "S"

#define DOUBLE_SERVICE_NAME "double_service"
#define DOUBLE_SERVICE_FORMAT "D"

#define FLOAT_SERVICE_NAME "float_service"
#define FLOAT_SERVICE_FORMAT "F"

#define XTRALONG_SERVICE_NAME "xtra_long_service"
#define XTRALONG_SERVICE_FORMAT "X"

#define STRING_INT_SERVICE_NAME "string_int_service"
#define STRING_INT_SERVICE_FORMAT "C:13;I"

#define INT_STRING_SERVICE_NAME "int_string_service"
#define INT_STRING_SERVICE_FORMAT "I:1;C"

#define ALL_VALUES_SERVICE_NAME "all_values_service"
#define ALL_VALUES_SERVICE_FORMAT "I:1;C:1;C:13;L:1;S:1;D:1;X:1;C:13;I:1;I:1;C:42"

#define STRING_SERVICE_VALUE_LENGTH 13
#define LONG_STRING_VALUE_LENGTH 42

int INT_SERVICE_VALUE = 42;
char CHAR_SERVICE_VALUE = 'a';
char STRING_SERVICE_VALUE[STRING_SERVICE_VALUE_LENGTH] = "Hello world !";
char LONG_STRING_VALUE[LONG_STRING_VALUE_LENGTH] = "Another string to try if it works or not !";
int LONG_SERVICE_VALUE = 2147483647;
short SHORT_SERVICE_VALUE = 32767;
double DOUBLE_SERVICE_VALUE = 42424242.42;
float FLOAT_SERVICE_VALUE = 42.42;
long XTRALONG_SERVICE_VALUE = 9223372036854775807;

typedef struct string_int_data{
  char str[STRING_SERVICE_VALUE_LENGTH];
  int value;
}__attribute__((packed)) string_int_t;
/*Remove padding of the struct because PyDIM does not handle padded struct*/

typedef struct int_string_data{
  int value;
  char str[LONG_STRING_VALUE_LENGTH];
}int_string_t;

typedef struct all_values_data{
  int integer1;
  char character;
  char hello_world[STRING_SERVICE_VALUE_LENGTH];
  int integer2;
  short short1;
  double double1;
  long xtralong1;
  char hello_world2[STRING_SERVICE_VALUE_LENGTH];
  int integer3;
  int integer4;
  char long_string[LONG_STRING_VALUE_LENGTH];
}__attribute__((packed)) all_values_t;

/*
* END OF SERVICES DEFINITION
*/

/*
* COMMANDS DEFINITION
*/
#define INT_CMND_NAME "int_cmnd"
#define CHAR_CMND_NAME "char_cmnd"
#define STRING_CMND_NAME "string_cmnd"
#define LONG_CMND_NAME "long_cmnd"
#define SHORT_CMND_NAME "short_cmnd"
#define DOUBLE_CMND_NAME "double_cmnd"
#define FLOAT_CMND_NAME "float_cmnd"
#define XTRALONG_CMND_NAME "xtralong_cmnd"
#define STRING_INT_CMND_NAME "string_int_cmnd"
#define INT_STRING_CMND_NAME "int_string_cmnd"

#define ALL_VALUES_CMND_NAME "all_values_cmnd"


void int_cmnd_callback(long *tag,int *address, int *size){
  printf("int_cmnd_callback received %d, size = %d\n\n",*address,*size);
}

void char_cmnd_callback(long *tag,char *address, int *size){
  printf("char_cmnd_callback received %c, size = %d\n\n",*address,*size);
}

void string_cmnd_callback(long *tag,char *address, int *size){
  char *received_data = (char *)malloc(sizeof(char) * STRING_SERVICE_VALUE_LENGTH);
  if(received_data != NULL){
    strncpy(received_data,address,STRING_SERVICE_VALUE_LENGTH);
    printf("string_cmnd_callback received %s\n, size = %d\n",received_data,*size);
    free(received_data);
  }else {
    printf("Error while mallocing char *\n");
  }
}

void long_cmnd_callback(long *tag, int *address, int *size){
  printf("long_cmnd_callback received %d, size = %d\n\n",*address,*size);
}

void short_cmnd_callback(long *tag, short *address,int *size){
  printf("short_cmnd_callback received %d, size = %d\n\n",*address,*size);
}

void double_cmnd_callback(long *tag, double *address, int *size){
  printf("double_cmnd_callback received %lf, size = %d\n\n",*address,*size);
}

void float_cmnd_callback(long *tag, float *address, int *size){
  printf("float_cmnd_callback received %f, size = %d\n\n",*address, *size);
}

void xtralong_cmnd_callback(long *tag, long*address, int *size){
  printf("xtralong_cmnd_service received %ld, size = %d\n\n",*address, *size);
}

void string_int_cmnd_callback(long *tag, void *address,int *size){
  int received_int;
  char *received_string = (char *)malloc(sizeof(char) * STRING_SERVICE_VALUE_LENGTH);
  if(received_string != NULL){
    strncpy(received_string,address,STRING_SERVICE_VALUE_LENGTH);
    printf("string_int_cmnd_callback received the string %s\n",received_string);
    free(received_string);
  }
  memcpy(&received_int,address+STRING_SERVICE_VALUE_LENGTH,sizeof(int));
  printf("and the integer %d, total size = %d\n\n",received_int,*size);
}

void int_string_cmnd_callback(long *tag, void *address, int *size){
  int received_int;
  memcpy(&received_int,address,sizeof(int));
  printf("int_string_cmnd_callback received the integer %d\n",received_int);
  char *received_string = (char *)malloc(sizeof(char) * LONG_STRING_VALUE_LENGTH);
  if(received_string != NULL){
    strncpy(received_string,address+4,LONG_STRING_VALUE_LENGTH);
    printf("and the string %s, total size = %d\n\n",received_string,*size);
    free(received_string);
  }
}

void all_values_cmnd_callback(long *tag, void *values, int *size){
  printf("all_values_cmnd_callback, size = %d values : \n",*size);
  all_values_t *values_received = (all_values_t*) values;
  printf(" - integer1 = %d\n",values_received->integer1);
  printf(" - character = %d\n",values_received->character);
  printf(" - hello_world = %.13s\n",values_received->hello_world);
  printf(" - integer2 = %d\n",values_received->integer2);
  printf(" - short1 = %d\n",values_received->short1);
  printf(" - double1 = %lf\n",values_received->double1);
  printf(" - xtralong1 = %ld\n",values_received->xtralong1);
  printf(" - hello_world2 = %.13s\n",values_received->hello_world2);
  printf(" - integer3 = %d\n",values_received->integer3);
  printf(" - integer4 = %d\n",values_received->integer4);
  printf(" - long_string = %s\n\n",values_received->long_string);
}

/*
* END OF COMMANDS DEFINITION
*/


int main(){
  /*Disable paddings in order to communicate with Python clients*/
  dic_disable_padding();
  dis_disable_padding();

  int int_service_id = dis_add_service(INT_SERVICE_NAME,INT_SERVICE_FORMAT,&INT_SERVICE_VALUE,sizeof(int),NULL,0);
  int char_service_id = dis_add_service(CHAR_SERVICE_NAME,CHAR_SERVICE_FORMAT,&CHAR_SERVICE_VALUE,sizeof(char),NULL,0);
  int string_service_id = dis_add_service(STRING_SERVICE_NAME,STRING_SERVICE_FORMAT,STRING_SERVICE_VALUE,sizeof(char)*strlen(STRING_SERVICE_VALUE),NULL,0);
  int long_service_id = dis_add_service(LONG_SERVICE_NAME,LONG_SERVICE_FORMAT,&LONG_SERVICE_VALUE,sizeof(int),NULL,0); /*L in DIM is 4 bytes, not 8*/
  int short_service_id = dis_add_service(SHORT_SERVICE_NAME,SHORT_SERVICE_FORMAT,&SHORT_SERVICE_VALUE,sizeof(short),NULL,0);
  int double_service_id = dis_add_service(DOUBLE_SERVICE_NAME,DOUBLE_SERVICE_FORMAT,&DOUBLE_SERVICE_VALUE,sizeof(double),NULL,0);
  int float_service_id = dis_add_service(FLOAT_SERVICE_NAME,FLOAT_SERVICE_FORMAT,&FLOAT_SERVICE_VALUE,sizeof(float),NULL,0);
  int xtralong_service_id = dis_add_service(XTRALONG_SERVICE_NAME,XTRALONG_SERVICE_FORMAT,&XTRALONG_SERVICE_VALUE,8,NULL,0);

  string_int_t string_int_service_value;
  strcpy(string_int_service_value.str,STRING_SERVICE_VALUE);
  string_int_service_value.value = 42;
  int string_int_service_id = dis_add_service(STRING_INT_SERVICE_NAME,STRING_INT_SERVICE_FORMAT,&string_int_service_value,strlen(STRING_SERVICE_VALUE)+sizeof(int),NULL,0);

  int_string_t int_string_service_value;
  int_string_service_value.value = 42;
  strcpy(int_string_service_value.str,LONG_STRING_VALUE);
  int int_string_service_id = dis_add_service(INT_STRING_SERVICE_NAME,INT_STRING_SERVICE_FORMAT,&int_string_service_value,sizeof(int)+strlen(LONG_STRING_VALUE),NULL,0);

  all_values_t all_values_service_data;
  all_values_service_data.integer1 = INT_SERVICE_VALUE;
  all_values_service_data.character = CHAR_SERVICE_VALUE;
  strcpy(all_values_service_data.hello_world,STRING_SERVICE_VALUE);
  all_values_service_data.integer2 = LONG_SERVICE_VALUE;
  all_values_service_data.short1 = SHORT_SERVICE_VALUE;
  all_values_service_data.double1 = DOUBLE_SERVICE_VALUE;
  all_values_service_data.xtralong1 = XTRALONG_SERVICE_VALUE;
  strcpy(all_values_service_data.hello_world2,STRING_SERVICE_VALUE);
  all_values_service_data.integer3 = INT_SERVICE_VALUE;
  all_values_service_data.integer4 = INT_SERVICE_VALUE;
  strcpy(all_values_service_data.long_string,LONG_STRING_VALUE);

  int all_values_service_id = dis_add_service(ALL_VALUES_SERVICE_NAME,ALL_VALUES_SERVICE_FORMAT,&all_values_service_data,sizeof(all_values_service_data),NULL,0);

  dis_add_cmnd(INT_CMND_NAME,INT_SERVICE_FORMAT,int_cmnd_callback,0);
  dis_add_cmnd(CHAR_CMND_NAME,CHAR_SERVICE_FORMAT,char_cmnd_callback,0);
  dis_add_cmnd(STRING_CMND_NAME,STRING_SERVICE_FORMAT,string_cmnd_callback,0);
  dis_add_cmnd(LONG_CMND_NAME,LONG_SERVICE_FORMAT,long_cmnd_callback,0);
  dis_add_cmnd(SHORT_CMND_NAME,SHORT_SERVICE_FORMAT,short_cmnd_callback,0);
  dis_add_cmnd(DOUBLE_CMND_NAME,DOUBLE_SERVICE_FORMAT,double_cmnd_callback,0);
  dis_add_cmnd(FLOAT_CMND_NAME,FLOAT_SERVICE_FORMAT,float_cmnd_callback,0);
  dis_add_cmnd(XTRALONG_CMND_NAME,XTRALONG_SERVICE_FORMAT,xtralong_cmnd_callback,0);
  dis_add_cmnd(STRING_INT_CMND_NAME,STRING_INT_SERVICE_FORMAT,string_int_cmnd_callback,0);
  dis_add_cmnd(INT_STRING_CMND_NAME,INT_STRING_SERVICE_FORMAT,int_string_cmnd_callback,0);
  dis_add_cmnd(ALL_VALUES_CMND_NAME,ALL_VALUES_SERVICE_FORMAT,all_values_cmnd_callback,0);

  dis_start_serving("server_test_c");

  while(1){
    dis_update_service(int_service_id);
    dis_update_service(char_service_id);
    dis_update_service(string_service_id);
    dis_update_service(long_service_id);
    dis_update_service(short_service_id);
    dis_update_service(double_service_id);
    dis_update_service(float_service_id);
    dis_update_service(xtralong_service_id);
    dis_update_service(string_int_service_id);
    dis_update_service(int_string_service_id);
    dis_update_service(all_values_service_id);

    sleep(2);
  }
  return 0;
}
