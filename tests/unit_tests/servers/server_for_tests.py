#!/bin/env python
# -*- coding: utf-8 -*-
import time
import pydim

def int_service(tag):
    return(42,)

def char_service(tag):
    return('a',)

def string_service(tag):
    return ("Hello world !",)

def long_service(tag):
    return (2147483647,)

def short_service(tag):
    return (32767,)

def double_service(tag):
    return (42424242.42,)

def float_service(tag):
    return (42.42,)

def xtra_long_service(tag):
    return (9223372036854775807,)

def string_int_service(tag):
    return ("Hello world !",42)

def int_string_service(tag):
    return (42,"Another string to try if it works or not !")

def all_values_service(tag):
    return (42,'a',"Hello world !",2147483647,32767,42424242.42,9223372036854775807,"Hello world !",42,42,"Another string to try if it works or not !")

def bytes_service(tag):
    return(b'as\xd8\xe1\xb7\xeb\xa8\xe5 \xd2\xb7\xe1',)

def bytes_string_int_service(tag):
    return(b"Hello world in bytes","Hello world !",42)

def all_cmnd_callback(value, tag):
    print("value received from client = "+repr(value))

INT_SERVICE_NAME="int_service"
INT_SERVICE_FORMAT="I:1"

CHAR_SERVICE_NAME = "char_service"
CHAR_SERVICE_FORMAT="C:1"

STRING_SERVICE_NAME = "string_service"
STRING_SERVICE_FORMAT="C"

LONG_SERVICE_NAME = 'long_service'
LONG_SERVICE_FORMAT = "L"

SHORT_SERVICE_NAME = 'short_service'
SHORT_SERVICE_FORMAT = "S"

DOUBLE_SERVICE_NAME = 'double_service'
DOUBLE_SERVICE_FORMAT = "D"

FLOAT_SERVICE_NAME = "float_service"
FLOAT_SERVICE_FORMAT = "F"

XTRALONG_SERVICE_NAME = "xtra_long_service"
XTRALONG_SERVICE_FORMAT = "X"

STRING_INT_SERVICE_NAME = "string_int_service"
STRING_INT_SERVICE_FORMAT = "C:13;I:1"

INT_STRING_SERVICE_NAME = "int_string_service"
INT_STRING_SERVICE_FORMAT = "I:1;C"

ALL_VALUES_SERVICE_NAME = "all_values_service"
ALL_VALUES_SERVICE_FORMAT = "I:1;C:1;C:13;L:1;S:1;D:1;X:1;C:13;I:1;I:1;C"

BYTES_SERVICE_NAME = "bytes_service"
BYTES_SERVICE_FORMAT = "C"

BYTES_STRING_INT_SERVICE_NAME = "bytes_string_int_service"
BYTES_STRING_INT_SERVICE_FORMAT = "C:20;C:13;I:1"

INT_CMND_NAME = "int_cmnd"
INT_CMND_FORMAT = INT_SERVICE_FORMAT

CHAR_CMND_NAME = "char_cmnd"
CHAR_CMND_FORMAT = CHAR_SERVICE_FORMAT

STRING_CMND_NAME = "string_cmnd"
STRING_CMND_FORMAT = STRING_SERVICE_FORMAT

LONG_CMND_NAME = "long_cmnd"
LONG_CMND_FORMAT = LONG_SERVICE_FORMAT

SHORT_CMND_NAME = "short_cmnd"
SHORT_CMND_FORMAT = SHORT_SERVICE_FORMAT

DOUBLE_CMND_NAME = "double_cmnd"
DOUBLE_CMND_FORMAT = DOUBLE_SERVICE_FORMAT

FLOAT_CMND_NAME = "float_cmnd"
FLOAT_CMND_FORMAT = FLOAT_SERVICE_FORMAT

XTRALONG_CMND_NAME = "xtralong_cmnd"
XTRALONG_CMND_FORMAT = XTRALONG_SERVICE_FORMAT

STRING_INT_CMND_NAME = "string_int_cmnd"
STRING_INT_CMND_FORMAT = STRING_INT_SERVICE_FORMAT

INT_STRING_CMND_NAME = "int_string_cmnd"
INT_STRING_CMND_FORMAT = INT_STRING_SERVICE_FORMAT

ALL_VALUES_CMND_NAME = "all_values_cmnd"
ALL_VALUES_CMND_FORMAT = ALL_VALUES_SERVICE_FORMAT

BYTES_CMND_NAME = "bytes_cmnd"
BYTES_CMND_FORMAT = BYTES_SERVICE_FORMAT

BYTES_STRING_INT_CMND_NAME = "bytes_string_int_cmnd"
BYTES_STRING_INT_CMND_FORMAT = BYTES_STRING_INT_SERVICE_FORMAT

class CmndSrv:
    CLASS_INT_CMND_NAME = "class_int_cmnd"
    def __init__(self):
        self.int_cmnd_id = pydim.dis_add_cmnd(CmndSrv.CLASS_INT_CMND_NAME, INT_CMND_FORMAT, self.cmnd_callback, 14)
    def cmnd_callback(self, value, tag):
        print("Class Value received from client", value)

def main():
    int_service_id = pydim.dis_add_service(INT_SERVICE_NAME,INT_SERVICE_FORMAT,int_service,1)
    char_service_id = pydim.dis_add_service(CHAR_SERVICE_NAME,CHAR_SERVICE_FORMAT,char_service,2)
    long_service_id = pydim.dis_add_service(LONG_SERVICE_NAME,LONG_SERVICE_FORMAT,long_service,3)
    short_service_id = pydim.dis_add_service(SHORT_SERVICE_NAME,SHORT_SERVICE_FORMAT,short_service,4)
    string_service_id = pydim.dis_add_service(STRING_SERVICE_NAME,STRING_SERVICE_FORMAT,string_service,5)
    double_service_id = pydim.dis_add_service(DOUBLE_SERVICE_NAME,DOUBLE_SERVICE_FORMAT,double_service,6)
    float_service_id = pydim.dis_add_service(FLOAT_SERVICE_NAME,FLOAT_SERVICE_FORMAT,float_service,7)
    xtra_long_service_id = pydim.dis_add_service(XTRALONG_SERVICE_NAME,XTRALONG_SERVICE_FORMAT,xtra_long_service,8)
    string_int_service_id = pydim.dis_add_service(STRING_INT_SERVICE_NAME,STRING_INT_SERVICE_FORMAT,string_int_service,9)
    int_string_service_id = pydim.dis_add_service(INT_STRING_SERVICE_NAME,INT_STRING_SERVICE_FORMAT,int_string_service,10)
    all_values_service_id = pydim.dis_add_service(ALL_VALUES_SERVICE_NAME,ALL_VALUES_SERVICE_FORMAT,all_values_service,11)
    bytes_service_id = pydim.dis_add_service(BYTES_SERVICE_NAME,BYTES_SERVICE_FORMAT,bytes_service,12)
    bytes_string_int_service_id = pydim.dis_add_service(BYTES_STRING_INT_SERVICE_NAME,BYTES_STRING_INT_SERVICE_FORMAT,bytes_string_int_service,13)


    int_command_id = pydim.dis_add_cmnd(INT_CMND_NAME,INT_CMND_FORMAT,all_cmnd_callback,1)
    char_command_id = pydim.dis_add_cmnd(CHAR_CMND_NAME,CHAR_CMND_FORMAT,all_cmnd_callback,2)
    string_command_id = pydim.dis_add_cmnd(STRING_CMND_NAME,STRING_CMND_FORMAT,all_cmnd_callback,3)
    long_command_id = pydim.dis_add_cmnd(LONG_CMND_NAME,LONG_CMND_FORMAT,all_cmnd_callback,4)
    short_command_id = pydim.dis_add_cmnd(SHORT_CMND_NAME,SHORT_CMND_FORMAT,all_cmnd_callback,5)
    double_command_id = pydim.dis_add_cmnd(DOUBLE_CMND_NAME,DOUBLE_CMND_FORMAT,all_cmnd_callback,6)
    float_command_id = pydim.dis_add_cmnd(FLOAT_CMND_NAME,FLOAT_CMND_FORMAT,all_cmnd_callback,7)
    xtralong_command_id = pydim.dis_add_cmnd(XTRALONG_CMND_NAME,XTRALONG_CMND_FORMAT,all_cmnd_callback,8)
    string_int_command_id = pydim.dis_add_cmnd(STRING_INT_CMND_NAME,STRING_INT_CMND_FORMAT,all_cmnd_callback,9)
    int_string_command_id = pydim.dis_add_cmnd(INT_STRING_CMND_NAME,INT_STRING_CMND_FORMAT,all_cmnd_callback,10)
    all_values_command_id = pydim.dis_add_cmnd(ALL_VALUES_CMND_NAME,ALL_VALUES_CMND_FORMAT,all_cmnd_callback,11)
    bytes_command_id = pydim.dis_add_cmnd(BYTES_CMND_NAME,BYTES_CMND_FORMAT,all_cmnd_callback,12)
    bytes_string_int_command_id = pydim.dis_add_cmnd(BYTES_STRING_INT_CMND_NAME,BYTES_STRING_INT_CMND_FORMAT,all_cmnd_callback,13)
    
    srv =  CmndSrv()

    pydim.dis_start_serving("Pydim_Python3_server")

    while True:
        pydim.dis_update_service(int_service_id)
        pydim.dis_update_service(char_service_id)
        pydim.dis_update_service(long_service_id)
        pydim.dis_update_service(short_service_id)
        pydim.dis_update_service(string_service_id)
        pydim.dis_update_service(double_service_id)
        pydim.dis_update_service(float_service_id)
        pydim.dis_update_service(xtra_long_service_id)
        pydim.dis_update_service(string_int_service_id)
        pydim.dis_update_service(int_string_service_id)
        pydim.dis_update_service(all_values_service_id)
        pydim.dis_update_service(bytes_service_id)
        pydim.dis_update_service(bytes_string_int_service_id)
        time.sleep(1)

if __name__ == '__main__':
    main()
